### Steps to run the app
I've used npm to create the react app instead of yarn. Please follow these steps to run the app:

Pre-requisites: Node v7 or above, npm v6 or above. Please make sure proxy has been correctly set for npm and terminal.

1. cd <project_folder>
2. npm i
3. npm start
4. Go to a browser and type localhost:3000
