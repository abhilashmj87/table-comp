import classes from './CsButton.module.css';

function CsButton(props) {
  const buttonClass = classes.buttonCore + ' ' + classes[props.size];
  return (
    <button className={buttonClass} disabled={props.disable} onClick={props.onclick}>{props.children}</button>
  );
}

export default CsButton;