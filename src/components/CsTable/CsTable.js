import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CsToolbar from '../CsToolbar/CsToolbar';
import CsButton from '../CsButton/CsButton';
import CsCheckbox from '../CsCheckbox/CsCheckbox';
import CsTableHead from '../CsTableHead/CsTableHead';
import CsTableBody from '../CsTableBody/CsTableBody';
import classes from './CsTable.module.css';
import DownloadImg from './resources/images/download.png';

const CsTable = (props) => {
  const [checkedItems, setCheckedItems] = useState({});
  const [isDisabled, setDisabled] = useState(true);
  const [noOfItems, setNoOfItems] = useState(0);
  const [checkAllChecked, setCheckAllChecked] = useState(false);

  const checkAllItems =(event) => {
    let tempObj = {};

    props.data.forEach((val, index)=>{
      tempObj[index] = event.target.checked;
    });

    calcToolbarValues(tempObj);
    setCheckedItems(tempObj);
  }

  const checkDetermination = (noOfCheckedItems) => {
    if (noOfCheckedItems === 0) {
      document.getElementById('interm').indeterminate = false;
      setCheckAllChecked(false);
      setDisabled(true);
    } else if (noOfCheckedItems === props.data.length) {
      document.getElementById('interm').indeterminate = false;
      setCheckAllChecked(true);
      setDisabled(false);
    } else {
      setCheckAllChecked(false);
      document.getElementById('interm').indeterminate = true;
      setDisabled(false);
    }
  }

  const calcToolbarValues = (obj) => {
    const objArr = Object.values(obj);
    let noOfCheckedItems = 0
    for(let i = 0 ; i < objArr.length ; i++) {
      if (objArr[i]) {
        noOfCheckedItems++;
      }
    }

    setNoOfItems(noOfCheckedItems);
    checkDetermination(noOfCheckedItems);
  }

  const processCheckedItems = (item, event) => {    
    let tempObj = {...checkedItems};
    tempObj[item] = event.target.checked;
    calcToolbarValues(tempObj);
    setCheckedItems(tempObj);
  }

  const processDownload = () => {
    // eslint-disable-next-line
    let downloadData = Object.entries(checkedItems).map(([key, value]) => {
      if (value) {
        if (typeof props.downloadCondition === 'function' && props.downloadCondition(props.data[key])) {
          return  props.downloadCondition(props.data[key]);
        } else {
          // TODO: Error handling. Maybe receive a callback from the user?
        }
      }
    });

    downloadData = downloadData.filter(data => data !== undefined);
    if(downloadData.length > 0) {
      alert(JSON.stringify(downloadData));
    } else {
      // TODO: Error handling. Maybe receive a callback from the user?
      alert("Nothing to download");
    }
  }

  const processHeaderColumns = () => {
    let columnsArray = props.headers || Object.keys(props.data[0]);
    props.allowDownload && columnsArray.splice(0, 0, "");
    return columnsArray;
  }

  const processStyleRelatedProps = () => {
    let cssProps = {};
    cssProps['--cs-table-width'] = props.width || 'auto';
    cssProps['--cs-table-height'] = props.height || 'auto';
    return cssProps;
  }

  return (
    <>
      {
        props.allowDownload && 
        <CsToolbar>
          <CsCheckbox id="interm" checked={checkAllChecked} onChange={checkAllItems}></CsCheckbox>
          <span className="cs-core-margin-lg">{
            noOfItems > 0 ? `Selected ${noOfItems}` : 'None selected'
          }</span>
          <CsButton onclick={processDownload} disable={isDisabled}>
            <img src={DownloadImg} alt="Download" className={classes.DownloadImg} />
            Download Selected
          </CsButton>
        </CsToolbar>
      }
      <table aria-label={props.ariaLabel || 'Table'} role="table" className={classes.CsTable} style={processStyleRelatedProps()}>
        <CsTableHead headers={processHeaderColumns()}></CsTableHead>
        <CsTableBody data={props.data} allowDownload={props.allowDownload} checkedItems={checkedItems} onCheckedItemsChange={processCheckedItems} customColumnRenderer={props.customColumnRenderer}></CsTableBody>
      </table>
    </>
  );
}

CsTable.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  allowDownload: PropTypes.bool,
  downloadCondition: PropTypes.func,
  customColumnRenderer: PropTypes.func
};

export default CsTable;
