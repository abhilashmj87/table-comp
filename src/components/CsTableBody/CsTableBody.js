import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import classes from './CsTableBody.module.css';
import CsCheckbox from '../CsCheckbox/CsCheckbox';
import CsTableCell from '../CsTableCell/CsTableCell';

const CsTableBody = (props) => {
  const [rowClasses, setRowClasses] = useState(classes.CsBodyRow);

  useEffect(() => {
    setRowClasses(classes.CsBodyRow)
  }, []);

  const processCheckedRow = (data, event) => {
    props.onCheckedItemsChange(data, event);
  };

  const TableCell = (tableCellProps) => {
    let dataArray = [];

    props.allowDownload && dataArray.push(
      <CsTableCell key={tableCellProps.rowIndex}>
        <CsCheckbox id={`tableCheck_${tableCellProps.rowIndex}`} checked={props.checkedItems[tableCellProps.rowIndex] || false} onChange={processCheckedRow.bind(null, tableCellProps.rowIndex)}></CsCheckbox>
      </CsTableCell>
    );
    Object.values(tableCellProps.rowData).forEach((data, index) => {
      if (props.customColumnRenderer) {
        dataArray.push(props.customColumnRenderer({data: tableCellProps.rowData, index: index}));
      } else {
        typeof data === 'string' && <CsTableCell key={data + '_' + index}>{data}</CsTableCell>
      }
    });

    return dataArray;
  };

  const TableRows = () => {
    return (
      props.data.map( (row, index) => (
        <tr role="row" key={index} className={ props.checkedItems && props.checkedItems[index] ? rowClasses + ' ' + classes.Selected : rowClasses}>
          <TableCell rowData={row} rowIndex={index}></TableCell>
        </tr>
      ))
    );
  };

  return (
    <tbody>
        <TableRows></TableRows>
    </tbody>
  );
}

CsTableBody.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object)
};

export default CsTableBody;
