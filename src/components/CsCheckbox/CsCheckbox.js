import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const CsCheckbox = (props) => {
  const [boxChecked, setChecked] = useState(!!props.checked);

  useEffect(() => {
    setChecked(props.checked)
  }, [props.checked]);

  const processChange = (event) => {
    if (props.checked === undefined || props.checked === false) {
      setChecked(event.target.checked);
    }
    return props.onChange(event);
  };

  return (
    <label htmlFor={props.id} title={props.label || props.id}>
      <input type="checkbox" id={props.id} checked={boxChecked || props.checked} onChange={processChange} />
    </label>
  )
}

CsCheckbox.propTypes = {
  checked: PropTypes.bool,
  partial: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
}

export default CsCheckbox;