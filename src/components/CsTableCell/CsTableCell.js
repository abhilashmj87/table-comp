import React from 'react';
import PropTypes from 'prop-types';
import classes from './CsTableCell.module.css';

const CsTableCell = (props) => {
    return (
      <td role="cell" className={classes.CsCellColumn + ' ' + props.className}>{props.children}</td>
    );
}

CsTableCell.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object)
};

export default CsTableCell;
