import React from 'react';
import PropTypes from 'prop-types';
import classes from './CsTableHead.module.css';

let CsTableHead = (props) => {
  const ColumnHeader = () => {
    return (
      props.headers.map( (col, index) => <th role="cell" key={index} className={classes.CsHeaderColumn}>{col}</th>) 
    );
  };

  return (
    <thead>
        <tr role="row" className={classes.CsHeaderRow}>
            <ColumnHeader />
        </tr>
    </thead>
  );
};

CsTableHead.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string)
};

export default CsTableHead;
