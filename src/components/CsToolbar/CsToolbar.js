import React from 'react';
import classes from './CsToolbar.module.css';


const CsToolbar = (props) => {
  return (
      <div className={classes.ToolbarCore} role="toolbar">
          {props.children}
      </div>
  );
}

export default CsToolbar;