import './App.css';
import GreenCircle from './green-circle.png'
import CsTable from './components/CsTable/CsTable';
import CsTableCell from './components/CsTableCell/CsTableCell';

function App() {
  const tabledata = [

    {name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled'},
    
    {name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available'},
    
    {name: 'uxtheme.dll', device: 'Lanniester', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll', status: 'available'},
    
    {name: 'cryptbase.dll', device: 'Martell', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll', status: 'scheduled'},
    
    {name: '7za.exe', device: 'Baratheon', path: '\\Device\\HarddiskVolume1\\temp\\7za.exe', status: 'scheduled'}
    
    ];
  
  const parseDownloadData = (data) => {
    if (data.status === 'available') {
      return [data.device, data.path];
    } else {
      return false;
    }
  };

  const columnRenderer = (renderData) => {
    const colName = Object.keys( renderData.data )[renderData.index];
    if (renderData.index === 2 && renderData.data.status === 'available') {
      return (
        <CsTableCell key={renderData.data.name + '_' + renderData.index}>
          <span>{renderData.data[colName]}</span>
          <img src={GreenCircle} className="AlignRight" alt="Available" />
        </CsTableCell>
      )
    } else if (renderData.index === 3) {
      return (
        <CsTableCell className="CapitalizeText" key={renderData.data.name + '_' + renderData.index}>{renderData.data[colName]}</CsTableCell>
      )
    } else {
      return (
        <CsTableCell key={renderData.data.name + '_' + renderData.index}>{renderData.data[colName]}</CsTableCell>
      )
    }
  };

  return (
    <div className="App">
      <CsTable columns={Object.keys(tabledata[0])} data={tabledata} width="80vw" allowDownload={true} downloadCondition={parseDownloadData} customColumnRenderer={columnRenderer}></CsTable>
    </div>
  );
}

export default App;
